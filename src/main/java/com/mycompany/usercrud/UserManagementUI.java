/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.usercrud;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import javax.swing.JOptionPane;

public class UserManagementUI extends javax.swing.JFrame {

    private final UserService userService;
    private ArrayList<User> userList;
    private boolean editMode = false;

    public UserManagementUI() {
        initComponents();
        userService = new UserService();
        updateTables();
    }

    private void updateTables() {
        userList = userService.getUserList();
        userTables.setModel(new CreateTables(userList));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        frameEditUser = new javax.swing.JFrame();
        labelLogin = new javax.swing.JLabel();
        labelName = new javax.swing.JLabel();
        labelGender = new javax.swing.JLabel();
        labelRole = new javax.swing.JLabel();
        confirmBtn = new javax.swing.JButton();
        loginTextField = new javax.swing.JTextField();
        nameTextField = new javax.swing.JTextField();
        cancelConfirmBtn = new javax.swing.JButton();
        genderComboBox = new javax.swing.JComboBox<>();
        roleComboBox = new javax.swing.JComboBox<>();
        messageDialog = new javax.swing.JDialog();
        addUserBtn = new javax.swing.JButton();
        editUserBtn = new javax.swing.JButton();
        deleteUserBtn = new javax.swing.JButton();
        userTablesPanel = new javax.swing.JScrollPane();
        userTables = new javax.swing.JTable();
        jMenuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuNewFile = new javax.swing.JMenuItem();

        frameEditUser.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        frameEditUser.setTitle("Add User");
        frameEditUser.setMinimumSize(new java.awt.Dimension(500, 300));
        frameEditUser.setName("Add User Frame"); // NOI18N
        frameEditUser.setResizable(false);

        labelLogin.setFont(new java.awt.Font("Helvetica Neue", 0, 16)); // NOI18N
        labelLogin.setText("Login :");

        labelName.setFont(new java.awt.Font("Helvetica Neue", 0, 16)); // NOI18N
        labelName.setText("Name :");

        labelGender.setFont(new java.awt.Font("Helvetica Neue", 0, 16)); // NOI18N
        labelGender.setText("Gender :");

        labelRole.setFont(new java.awt.Font("Helvetica Neue", 0, 16)); // NOI18N
        labelRole.setText("Role :");

        confirmBtn.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        confirmBtn.setText("Add");
        confirmBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmBtnActionPerformed(evt);
            }
        });

        loginTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginTextFieldActionPerformed(evt);
            }
        });

        nameTextField.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        nameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTextFieldActionPerformed(evt);
            }
        });

        cancelConfirmBtn.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        cancelConfirmBtn.setText("Cancel");
        cancelConfirmBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelConfirmBtnActionPerformed(evt);
            }
        });

        genderComboBox.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        genderComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female", "Other" }));
        genderComboBox.setSelectedIndex(-1);
        genderComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genderComboBoxActionPerformed(evt);
            }
        });

        roleComboBox.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        roleComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CEO", "Staff Manager", "Employee" }));
        roleComboBox.setSelectedIndex(-1);

        javax.swing.GroupLayout frameEditUserLayout = new javax.swing.GroupLayout(frameEditUser.getContentPane());
        frameEditUser.getContentPane().setLayout(frameEditUserLayout);
        frameEditUserLayout.setHorizontalGroup(
            frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameEditUserLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(frameEditUserLayout.createSequentialGroup()
                        .addComponent(cancelConfirmBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(confirmBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(frameEditUserLayout.createSequentialGroup()
                            .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(labelGender)
                                .addComponent(labelRole)
                                .addComponent(labelName))
                            .addGap(18, 18, 18)
                            .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(roleComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frameEditUserLayout.createSequentialGroup()
                                    .addGap(0, 0, Short.MAX_VALUE)
                                    .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(genderComboBox, javax.swing.GroupLayout.Alignment.TRAILING, 0, 362, Short.MAX_VALUE)
                                        .addComponent(nameTextField, javax.swing.GroupLayout.Alignment.TRAILING)))))
                        .addGroup(frameEditUserLayout.createSequentialGroup()
                            .addComponent(labelLogin)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(loginTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(24, 24, 24))
        );
        frameEditUserLayout.setVerticalGroup(
            frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frameEditUserLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelLogin)
                    .addComponent(loginTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelName)
                    .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelGender)
                    .addComponent(genderComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelRole)
                    .addComponent(roleComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(frameEditUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(confirmBtn)
                    .addComponent(cancelConfirmBtn))
                .addGap(24, 24, 24))
        );

        frameEditUser.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout messageDialogLayout = new javax.swing.GroupLayout(messageDialog.getContentPane());
        messageDialog.getContentPane().setLayout(messageDialogLayout);
        messageDialogLayout.setHorizontalGroup(
            messageDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        messageDialogLayout.setVerticalGroup(
            messageDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        addUserBtn.setText("Add");
        addUserBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addUserBtnActionPerformed(evt);
            }
        });

        editUserBtn.setText("Edit");
        editUserBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editUserBtnActionPerformed(evt);
            }
        });

        deleteUserBtn.setText("Delete");
        deleteUserBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteUserBtnActionPerformed(evt);
            }
        });

        userTablesPanel.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N

        userTables.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        userTables.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        userTables.setShowGrid(false);
        userTables.getTableHeader().setReorderingAllowed(false);
        userTablesPanel.setViewportView(userTables);
        userTables.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        menuFile.setText("File");

        menuNewFile.setText("New File");
        menuNewFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuNewFileActionPerformed(evt);
            }
        });
        menuFile.add(menuNewFile);

        jMenuBar.add(menuFile);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(398, 398, 398)
                        .addComponent(addUserBtn)
                        .addGap(18, 18, 18)
                        .addComponent(editUserBtn)
                        .addGap(18, 18, 18)
                        .addComponent(deleteUserBtn))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(userTablesPanel)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(userTablesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addUserBtn)
                    .addComponent(editUserBtn)
                    .addComponent(deleteUserBtn))
                .addGap(18, 18, 18))
        );

        getAccessibleContext().setAccessibleName("User Management System");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void addUserBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addUserBtnActionPerformed
        // TODO add your handling code here:
        editMode = false;
        frameEditUser.setVisible(true);
    }//GEN-LAST:event_addUserBtnActionPerformed

    private void editUserBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editUserBtnActionPerformed
        // TODO add your handling code here:
        if (userTables.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Please select a row first!", "Row is not selected yet", JOptionPane.ERROR_MESSAGE);
        } else {
            editMode = true;
            int selectedRow = userTables.getSelectedRow();
            User oldUser = userList.get(selectedRow);
            loginTextField.setText(oldUser.getLogin());
            nameTextField.setText(oldUser.getName());
            genderComboBox.setSelectedItem(oldUser.getGender());
            roleComboBox.setSelectedItem(oldUser.getRole());
            confirmBtn.setText("Save");
            frameEditUser.setVisible(true);
        }
    }//GEN-LAST:event_editUserBtnActionPerformed

    private void deleteUserBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteUserBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow = userTables.getSelectedRow();
        if (selectedRow != -1) {
            confirmOption = new JOptionPane("Are you sure to delete this user ?", JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            messageDialog = confirmOption.createDialog(this, "Confirm Delete");
            messageDialog.setVisible(true);
            int userChoice = (int) confirmOption.getValue();
            if (userChoice == JOptionPane.OK_OPTION) {
                userList.remove(selectedRow);
                updateTables();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a row first!", "Row is not selected yet", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_deleteUserBtnActionPerformed

    private void menuNewFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuNewFileActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuNewFileActionPerformed

    private void cancelConfirmBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelConfirmBtnActionPerformed
        clearFormAddUser();
        frameEditUser.dispose();
    }//GEN-LAST:event_cancelConfirmBtnActionPerformed

    private void clearFormAddUser() {
        // TODO add your handling code here:
        loginTextField.setText("");
        nameTextField.setText("");
        genderComboBox.setSelectedIndex(-1);
        roleComboBox.setSelectedIndex(-1);
    }

    private void nameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTextFieldActionPerformed

    private void loginTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_loginTextFieldActionPerformed

    private void confirmBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmBtnActionPerformed
        // TODO add your handling code here:
        User user = new User(userService.getLastId(), loginTextField.getText(), nameTextField.getText(), (String) genderComboBox.getSelectedItem(), (String) roleComboBox.getSelectedItem());
        if (!editMode) {
            userService.incrementLastId();
            userList.add(user);
        } else {
            userList.set(userTables.getSelectedRow(), user);
        }
        updateTables();
        clearFormAddUser();
        frameEditUser.dispose();

    }//GEN-LAST:event_confirmBtnActionPerformed

    private void genderComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genderComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_genderComboBoxActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UserManagementUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UserManagementUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UserManagementUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UserManagementUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UserManagementUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addUserBtn;
    private javax.swing.JButton cancelConfirmBtn;
    private javax.swing.JButton confirmBtn;
    private javax.swing.JButton deleteUserBtn;
    private javax.swing.JButton editUserBtn;
    private javax.swing.JFrame frameEditUser;
    private javax.swing.JComboBox<String> genderComboBox;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JLabel labelGender;
    private javax.swing.JLabel labelLogin;
    private javax.swing.JLabel labelName;
    private javax.swing.JLabel labelRole;
    private javax.swing.JTextField loginTextField;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenuItem menuNewFile;
    private javax.swing.JDialog messageDialog;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JComboBox<String> roleComboBox;
    private javax.swing.JTable userTables;
    private javax.swing.JScrollPane userTablesPanel;
    // End of variables declaration//GEN-END:variables
    private JOptionPane confirmOption;

    private class CreateTables extends AbstractTableModel {

        private final ArrayList<User> userList;

        public CreateTables(ArrayList<User> userList) {
            this.userList = userList;
        }

        @Override
        public int getRowCount() {
            return userService.getSize();
        }

        @Override
        public int getColumnCount() {
            return 5;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            User user = userList.get(rowIndex);

            switch (columnIndex) {
                case 0:
                    return user.getId();
                case 1:
                    return user.getLogin();
                case 2:
                    return user.getName();
                case 3:
                    return user.getGender();
                case 4:
                    return user.getRole();
            }
            return null;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return "ID";
                case 1:
                    return "Login";
                case 2:
                    return "Name";
                case 3:
                    return "Gender";
                case 4:
                    return "Role";
            }
            return null;
        }
    }
}
